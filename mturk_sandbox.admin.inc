<?php

/**
 * @file
 * Administrative page callbacks for the mturk_sandbox module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function mturk_sandbox_admin_settings_form($form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['account']['mturk_sandbox_aws_access_key_id'] = array(
    '#title' => t('AWS Access Key ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('mturk_sandbox_aws_access_key_id', 'NOT SET'),
    '#size' => 15,
    '#required' => TRUE,
    '#description' => t('Description to be set later'),
  );

  $form['account']['mturk_sandbox_aws_secret_access_key'] = array(
    '#title' => t('AWS Secret Access Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('mturk_sandbox_aws_secret_access_key', 'NOT SET'),
    '#size' => 15,
    '#required' => TRUE,
    '#description' => t('Description to be set later'),
  );
  return system_settings_form($form);
}
